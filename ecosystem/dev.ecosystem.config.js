module.exports = {
    apps:[
        {
            name:"APP1",
            script:"./app/bin/www",
            watch:[
                "./app/*"
            ],
            ignore_watch:[
                "./node_modules/",
                "./app/ecosystem",
                "./app/views"
            ]
        }
    ]
};