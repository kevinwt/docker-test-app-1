FROM node:14-slim as debug
WORKDIR /work
COPY ["package.json","/work/package.json"]
RUN npm install
RUN npm install -g nodemon
COPY . /work/app
ENTRYPOINT ["nodemon","--inspect=0.0.0.0:9229","-L","./app/bin/www"]

FROM node:14-slim as prod
WORKDIR /work
COPY ["package.json","/work/package.json"]
RUN npm install
COPY . /work/app
CMD ["node","./app/bin/www"]