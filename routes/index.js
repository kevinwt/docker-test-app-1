var express = require('express');
var router = express.Router();
var mainModel = require('../models/index');

router.get('/view/message', (req, res, next) => {
  return res.render('messages',{});
});

router.get('/api/message', (req, res, next) => {
  mainModel.getMessages()
  .then(result => {
    return res.send(result);
  })
  .catch(err => {
    return res.status(500).send(err);
  });
});

router.post('/api/message', (req, res, next) => {
  console.log(req.body);
  mainModel.postMessage(req.body)
  .then(result => {
    return res.send(result);
  })
  .catch(err => {
    return res.status(500).send(err);
  });
});

/* GET home page. */
router.get('/', (req, res, next) => {
  mainModel.setIdentity()
  .then(result => {
    return res.render('index', { 
      title: result['title'],
      name:result['name']
    });
  })
  .catch(err => {
    console.error(err);
    return res.status(500).send(err);
  });
});

module.exports = router;
