const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { promisify } = require('util');
const redis = require('redis');
const client = redis.createClient({
    url:`redis://app1_redis:6379`
});

const getAsync = promisify(client.get).bind(client);
// const delAsync = promisify(client.del).bind(client);
// const setAsync = promisify(client.set).bind(client);

const {
    MONGO_USERNAME,
    MONGO_PASSWORD,
    MONGO_HOSTNAME,
    MONGO_PORT,
    MONGO_DB
} = process.env;

const options = {
    useNewUrlParser: true,
    reconnectInterval: 500,
    connectTimeoutMS: 10000,
    useUnifiedTopology: true
};

const url = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}?authSource=admin`;

const ident = new Schema({
    author:Schema.ObjectId,
    title:'string',
    name:'string'
});

const msg = new Schema({
    msg_id:Schema.ObjectId,
    msg:'string',
    handle:'string'
});

class Main{

    constructor(){}

    connect(){
        return mongoose.createConnection(url, options);
    }

    async setIdentity(){
        const connection = mongoose.createConnection(url, options);
        const identity = connection.model('Identity', ident);

        const existingIdent = await identity.find();
        console.log('Existing:',existingIdent);
        if(!(existingIdent || []).length){
            const createdIdent = await identity.create({title:'App 1',name:'app1'});
            console.log('Created:',createdIdent);
            connection.close();
            return createdIdent;
        }else{
            connection.close();
            return existingIdent[0];
        }
    }

    async getIdentity(){

    }

    async postMessage(data={}){
        try{
            const connection = mongoose.createConnection(url, options);
            const message = connection.model('Message', msg);

            const createdMessage = await message.create(data);
            connection.close();
            await promisify(client.del('messages'));
            return createdMessage;
        }catch(err){
            connection.close();
            console.error(err);
            return err;
        }
    }

    async getMessages(){
        try{
            console.log('Changed!');
            let redisMsg = await getAsync('messages');

            if(redisMsg){
                return redisMsg || [];
            }

            const connection = mongoose.createConnection(url, options);
            const message = connection.model('Message', msg);
            const existingMessages = await message.find();
            connection.close();
            client.set('messages', JSON.stringify(existingMessages));
            return existingMessages;
        }catch(err){
            console.error(err);
            return err;
        }
    }

}


module.exports = new Main();